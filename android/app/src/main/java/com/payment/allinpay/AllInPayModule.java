package com.bindo.allinpay;

// Android packages
import android.util.Log;
import android.app.Activity;
import android.content.Intent;
import android.content.ComponentName;
import android.os.Bundle;

// React packages
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ActivityEventListener;

// Allinpay packages
import com.allinpay.usdk.core.data.RequestData;
import com.allinpay.usdk.core.data.ResponseData;
import com.allinpay.usdk.core.data.BaseData;


public class AllInPayModule extends ReactContextBaseJavaModule {
    static final int SEND_USDK_REQUEST = 6;
    private Callback mResultCallback;
    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            Log.d("AllInPayModule", Integer.toString(requestCode));
            if (requestCode == SEND_USDK_REQUEST) {
                Bundle extras = data.getExtras();
                if (extras == null) {
                    Log.e("", "No extras provided");
                    return;
                }
                ResponseData respone = (ResponseData) extras
                    .getSerializable(ResponseData.KEY_ERTRAS);
                StringBuilder builder = new StringBuilder("Result: ");
                builder.append(respone.getValue(BaseData.REJCODE_CN));
                builder.append("return code:");
                builder.append(resultCode);
                mResultCallback.invoke(builder.toString());
            }
        }
    };

    public AllInPayModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "AllInPay";
    }

    @ReactMethod
    public void startPay(String cardtype, String amount, Callback resultCallbcak) {
        Activity currentActivity = getCurrentActivity();
        mResultCallback = resultCallbcak;
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.allinpay.usdk",
                "com.allinpay.usdk.MainActivity"));

            Bundle bundle = new Bundle();
            RequestData data = new RequestData();

            data.putValue(RequestData.AMOUNT, amount);
            data.putValue(RequestData.CARDTYPE, cardtype);
            data.putValue(RequestData.TRANSTYPE, BaseData.TRANSTYPE_SALE);

            bundle.putSerializable(RequestData.KEY_ERTRAS, data);
            intent.putExtras(bundle);
            Log.d("AllInPayModule","Amount is" + amount);
            currentActivity.startActivityForResult(intent, SEND_USDK_REQUEST);
        }
        catch (Exception e) {
            Log.d("AllInPayModule","exception happend in start activity");
        }
    }
}
