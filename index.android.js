/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import update from 'immutability-helper';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  NativeModules
} from 'react-native';

var paymentApp = NativeModules.AllInPay;
const AMOUNT_DIGITIS = 12;
const BANK_CARD = "001";
const QR_CODE = "100";

export default class payment extends Component {
  constructor(props) {
    super(props);
    this.startPay = this.startPay.bind(this);
    this.normalizeAmount = this.normalizeAmount.bind(this);
    this.checkAmount = this.checkAmount.bind(this);
    this.getPaymentResult = this.getPaymentResult.bind(this);
    this.state = {amount: 0, showErr: false, paymentResult: ''};
  }

  startPay(cardType) {
    this.setState(update(this.state, {amount: {$set: 0}}));
    let amount = this.normalizeAmount(this.state.amount);
    paymentApp.startPay(cardType, amount, this.getPaymentResult);
    this.setState({amount: 0, showErr: false});
  }

  getPaymentResult(resultStr) {
    this.setState(update(this.state, {paymentResult: {$set: resultStr}}));
  }

  normalizeAmount(amount) {
    // amount unit is cent and should be 12 digits
    let result = (amount * 100).toString();
    let paddingDigits = AMOUNT_DIGITIS - result.length;
    return new Array(paddingDigits+1).join("0") + result;
  }

  checkAmount(amountStr) {
    this.setState(update(this.state, {showErr: {$set: false}}));
    let re = /^((\d)+|(\d)+\.(\d){1,2})$/;
    let amount = parseFloat(amountStr);
    if (!re.test(amountStr) || amount === 0 || amount > 9999999999) {
      this.setState(update(this.state, {showErr: {$set: true}}));
      return;
    }
    this.setState(
      update(this.state,
          {amount: {$set: amount}, showErr: {$set: false}}));
  }

  render() {
    let inputErr = this.state.showErr?
      (<Text>Please input the right format of money, more than 0 and less than 10 billion</Text>):
      null;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Bindo Pay
        </Text>
        <TextInput
        keyboardType="numeric"
        autoFocus={true}
        style={styles.amountInput}
        onChangeText={this.checkAmount}
        value={this.state.amount.toString()}
        />
        {inputErr}
        <Button disabled={this.state.showErr} style={styles.cardPayment} onPress={() => this.startPay(BANK_CARD)} title="Card Payment"
          accessibilityLabel="Card Payment"/>
        <Button disabled={this.state.showErr} style={styles.qrPayment} onPress={() => this.startPay(QR_CODE)} title="QR Payment"
          accessibilityLabel="QR Code Payment"/>
        <Text>{this.state.paymentResult}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  cardPayment: {
    textAlign: 'center',
    color: '#4286f4',
  },
  qrPayment: {
    textAlign: 'center',
    color: '#e6ccff',
  },
  amountInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1
  }
});

AppRegistry.registerComponent('payment', () => payment);
